# Clean By Date #

Just a simple Python script to walk a directory structure and delete files
older than a certain number of days.  **Empty directories will also be deleted.**

    $ python cleanbydate.py -h
    usage: cleanbydate.py [-h] [--debug] directory days

    Walk a directory structure and delete files older (havent been modified) than
    input number of days

    positional arguments:
      directory   top level directory to walk through
      days        Delete files older (not modified) than this number of days

    optional arguments:
      -h, --help  show this help message and exit
      --debug     Run in debug mode: mock run where no files will be deleted.


